import logging

import uvicorn as uvicorn
from fastapi import FastAPI
from fastapi.responses import ORJSONResponse

from api.v0 import interface
from core.config import ApiSetting, ModelSetting
from core.logging import LOGGING

api_settings = ApiSetting()
model_setting = ModelSetting()

app = FastAPI(
    docs_url="/api/openapi",
    openapi_url="/api/openapi.json",
    default_response_class=ORJSONResponse,
)


app.include_router(interface.router, prefix="/api/v0/interface")

if __name__ == "__main__":
    uvicorn.run(
        "main:app",
        host=api_settings.host,
        port=api_settings.port,
        reload=api_settings.debug,
        log_level=logging.DEBUG,
        log_config=LOGGING,
    )
