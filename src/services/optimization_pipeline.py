import typing as tp
from abc import ABC, abstractmethod

import numpy as np
import pandas as pd
from pypfopt import (DiscreteAllocation, EfficientFrontier,
                     objective_functions, risk_models)

from services.model import IModel


class IPortOptimizer(ABC):
    @abstractmethod
    def optimize(self, data: tp.Any, money: float) -> tuple[dict[str, int], float]:
        ...


def create_gmv_portfolio(data: pd.DataFrame) -> EfficientFrontier:
    "Return model of GMV portfolio"

    S = risk_models.CovarianceShrinkage(data).ledoit_wolf()
    ef = EfficientFrontier(None, S, weight_bounds=(0, 1))
    ef.min_volatility()
    return ef


def create_gmv_l2_portfolio(data: pd.DataFrame) -> EfficientFrontier:
    "Return model of GMV with transaction cost and L2 reg portfolio"
    len_tickers = data.shape[1]
    initial_weights = np.array([1 / len_tickers] * len_tickers)
    S = risk_models.CovarianceShrinkage(data).ledoit_wolf()
    ef = EfficientFrontier(None, S, weight_bounds=(0, 1))
    ef.add_objective(
        objective_functions.transaction_cost, w_prev=initial_weights, k=0.0004
    )
    ef.add_objective(objective_functions.L2_reg, gamma=0.05)
    ef.min_volatility()
    return ef


def get_last_prices(tickers_in_model: list[str], data: pd.DataFrame) -> tp.Any:
    return data[tickers_in_model].iloc[-1]


class ClassificationBasedOptimizerGMV(IPortOptimizer):
    def __init__(
        self,
        model: IModel,
    ):
        self.model = model

    def optimize(
        self, data: pd.DataFrame, money: float
    ) -> tuple[dict[str, int], float]:
        tr_data = self.model.transform(data)
        filter = [k for k, v in self.model.predict(tr_data).items() if v == 1]
        ef = create_gmv_portfolio(data[filter])
        weights = ef.clean_weights()
        tickers_in_model = [str(x) for x in weights.keys()]
        latest_prices = get_last_prices(tickers_in_model, data=data)

        # Allocate money to tickers
        da = DiscreteAllocation(
            weights,
            latest_prices,
            total_portfolio_value=int(money),
            short_ratio=0.3,
        )
        alloc, leftover = da.greedy_portfolio()
        # print(alloc, leftover)
        output = {k: round(latest_prices[k] * v, 2) for k, v in alloc.items()}
        return (
            output,
            float(leftover),
        )


class ClassificationBasedOptimizerGMVL2(IPortOptimizer):
    def __init__(
        self,
        model: IModel,
    ):
        self.model = model

    def optimize(
        self, data: pd.DataFrame, money: float
    ) -> tuple[dict[str, int], float]:
        tr_data = self.model.transform(data)
        filter = [k for k, v in self.model.predict(tr_data).items() if v == 1]
        ef = create_gmv_l2_portfolio(data[filter])
        weights = ef.clean_weights()
        tickers_in_model = [str(x) for x in weights.keys()]
        latest_prices = get_last_prices(tickers_in_model, data=data)

        # Allocate money to tickers
        da = DiscreteAllocation(
            weights,
            latest_prices,
            total_portfolio_value=int(money),
            short_ratio=0.3,
        )
        alloc, leftover = da.greedy_portfolio()
        # print(alloc, leftover)
        output = {k: round(latest_prices[k] * v, 2) for k, v in alloc.items()}
        return (
            output,
            float(leftover),
        )
